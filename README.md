# Happy restic

An opinionated wizard to set up a restic-based backup. Made to be used on ubuntu. 

To use it, 
- install the restic snap
- make the provided `hre` bash script executable and call it over and over.
  It will guide you through stuff.
